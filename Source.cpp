#include <iostream>
#include <string>
#include <fstream> // for FILE stuff
#include "BinarySearchTree.h"
#include "AVL.h"
#include "BinaryMaxHeap.h"
//NABILA_NAZAR: A00044909 is my student number.

using namespace std;

void main()
{

	//======== QUESTION 1 ========
	// AVL Tree (Input file + Output file)
	// Tree Level traversal...

	ifstream readFile; // Declare the function to read the file, it can be named anything.
	readFile.open("input-q1a2.txt");

	//READING from this specific txt file.
	//REMEMBER: When reading or writing into a file OPEN and CLOSE functions must be writtin.
	// OPEN a file, read/write do whatever is desired, then CLOSE it because YA DONE with it (and it wont work if you dont).

	int numberOfNumbers; // this would be 6 (representing the 6 numbers in the second line).
	readFile >> numberOfNumbers; // This works like cin.
	// END of file explanation, thank you Matt :)

	// =========================================================================

	//Finally, inserting all of this and putting it together.

	AVL avl1;
	vector<int> nums; // NUMS = the numbers in the second line of the input.txt (these are the numbers we want to insert into the file).
	for (int i = 1; i <= numberOfNumbers; i++)
	{
		int temp; // Temporary (free space) for the numbers to be inserted in.
		readFile >> temp; //NOTE spaces and endlines are skipped in file reading ;)
		avl1.insert(new FruitNode(temp, " ")); // Combining the contents of the input file with the avl insert function.
	}
	readFile.close();

	cout << "Number of numbers in file: " << numberOfNumbers << endl;
	for (int i = 0; i < nums.size(); i++)
	{
		cout << nums[i] << " Node";
	}
	cout << endl;

	// For reference there are 6 numbers.
	// The numbers in the second line are 6, 12, 24, 36, 42, 30 (not in order).

	// --------------------------------------------
	// Now we need to traverse the tree's levels.
	// What I did for show.bst I did the same for avl with showAVL.
	avl1.showAVL(avl1.root); // This outputs the AVL tree + Levels.

	cout << "Please check output.txt file for results,\nit should display each level + the word NODE.\n" << endl;

	//  Q1, done. 
	// ================================================================================================

	//======== QUESTION 2 ========

	ifstream readFile2; // Reading the file for this function.
	// For your reference the .txt file contents are this:
	// 5
	// 19 29 39 9 49

	readFile2.open("heap-input-q2a2.txt"); // Opening that file.
	int numOfnums; // this is 5 (5 numbers)
	readFile2 >> numOfnums;

	// IMPLEMENTING the Binary Max heap. + inserting input numbers from .txt
	BinaryMaxHeap maxHeap; 
	vector<int> numbers; 
	for (int i = 1; i <= numOfnums; i++)
	{
		int temp; // Temporary (free space) for the numbers to be inserted in.
		readFile2 >> temp; //NOTE spaces and endlines are skipped in file reading ;)
		maxHeap.Insert(FruitNode(temp, " Node"));
	}

	readFile2.close();
	// ^ Repeated all the same steps, simply renamed the variables to avoid confusion.
	
	// Showing the heap to make sure it was inserted.
	maxHeap.showHeap(); // IT WORKS!!

	cout << "Show Heap outputting in the output file!!\nPlease check the .txt :D\n" << endl;

	system("pause");

}
