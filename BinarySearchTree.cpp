#include "BinarySearchTree.h"
#include <fstream> // for FILE stuff

void BinarySearchTree::insert(FruitNode* newFruit)
{

    // if tree empty, this fruit can be the root.
    if (root == NULL)
    {
        root = newFruit;
        return; //exit funciton
    }

    // some pointers to help us navigate the tree
    FruitNode* current = root;
    FruitNode* parent = NULL; //Parent of current (previously visited NODE.)

    while (true) // INFILTRATE THE ROOT!!
    {
        //parent keeps track of last visited
        parent = current;
        // left...or right?

        //if newStudentID is less than ROOTS go left.
        if (newFruit->fruitPrice < current->fruitPrice)
        {
            current = current->leftChild;
            // if current is NULL, we've found a free spot to insert this NODE.
            if (current == NULL)
            {
                parent->leftChild = newFruit;
                return; //DONE.
            }
        }
        else //goes RIGHT
        {
            current = current->rightChild;
            if (current == NULL)
            {
                parent->rightChild = newFruit;
                return; //DONE.
            }
        }

    }
}

FruitNode* BinarySearchTree::search(int FruitID, bool showSearchPath)
{

    //if tree empty, cant find a match, ever (lol)
    if (root == NULL)
    {
        return NULL;
    }

    FruitNode* current = root;

    // keep looping until we find a match.

    while (current->fruitPrice != FruitID)
    {

        if (showSearchPath)
        {
            cout << current->fruitPrice << " " << current->name << endl;

            //go down left of right child?
            if (FruitID < current->fruitPrice)
            {
                //go Left
                current = current->leftChild;
            }
            else // goes right
            {
                current = current->rightChild;
            }

            //IF current node is NULL, then its not in the tree, won't find it.
            if (current == NULL)
            {
                return NULL;
            }
        }
    }

    // if we got here, we found our student in the tree, yippie kay yay!
    return current;
}

void BinarySearchTree::inOrderTraversal(FruitNode* current)
{
    // ends of branches we hit NULLS, and that helps us break the recursive LOOP.
    if (current != NULL)
    {
        inOrderTraversal(current->leftChild);
        cout << current->fruitPrice << " " << current->name << endl;
        inOrderTraversal(current->rightChild);
    }
   

}

void BinarySearchTree::preOrderTraversal(FruitNode* current)
{

    if (current != NULL)
    {
        cout << current->fruitPrice << " " << current->name << endl;
        preOrderTraversal(current->leftChild);
        preOrderTraversal(current->rightChild);
    }
}

void BinarySearchTree::postOrderTraversal(FruitNode* current)
{

    if (current != NULL)
    {
        postOrderTraversal(current->leftChild);
        postOrderTraversal(current->rightChild);
        cout << current->fruitPrice << " " << current->name << endl;
    }
}

// Referenced Algorithm: Matt's Tree LEVEL thing + geeks4geeks (to recheck and learn).
void BinarySearchTree::show(FruitNode* p)
{
    /*NOTE*/
    //Instead of COUT make it display in an output file like the assessment wanted.
    ofstream writeFile;
    writeFile.open("output-q1-a2.txt");

    // Root is empty, base case.
    if (root == NULL) return;

    // Creates an empty queue for the level order traversal.
    queue<FruitLevelNode> q;

     // Enqueue (add) the root into the queue and initialise the height with the constructor you just built.
    q.push(FruitLevelNode(root, 0));

    int previousOutputLevel = -1;

    while (q.empty() == false)
    {
        //Print the FRONT of the queue and then dequeue (remove) it from the queue.
        FruitLevelNode node = q.front();
        if (node.level != previousOutputLevel)
        {
            writeFile << endl;
            writeFile << node.level << "- ";
            previousOutputLevel = node.level;
        }
        writeFile << node.fruit->fruitPrice << ":" << node.fruit->name << " ";
        q.pop();


        /* For: Enqueing the RIGHT child */
        if (node.fruit->rightChild != NULL)
            q.push(FruitLevelNode(node.fruit->rightChild, node.level + 1));

        /* For: Enqueing the LEFT child */
        //Same thing, replace with the word left.
        if (node.fruit->leftChild != NULL)
            q.push(FruitLevelNode(node.fruit->leftChild, node.level + 1));
    }
    writeFile.close();

}
