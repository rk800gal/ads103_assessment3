#pragma once
#include <queue> // used to add DISPLAY BST type of function.
#include "FruitNode.h"
#include <fstream>

// Making a constructor to help show the tree levels!
// Or a "HELPER CLASS FOR OUTPUT".
class FruitLevelNode
{
public:
	FruitNode* fruit;
	int level; // This is the level variable.

	// The constructor:
	FruitLevelNode(FruitNode* fruit, int level)
	{
		this->fruit = fruit;
		this->level = level;
	}

};


// The Root node acts as the centre of the tree
//-------------
// **REMEMBER**
// Everything SMALLER than root --> inserts on LEFT!
// Everything BIGGER than root --> inserts on RIGHT

class BinarySearchTree
{
public:
	FruitNode* root = NULL; // The tree starts as an empty root.
	void insert(FruitNode* newFruit);
	// showSearchPath has a default value setup for if we dont pass in a value for it

	FruitNode* search(int fruitPrice, bool showSearchPath = false);

	// The Recursive traversal functions used to rotate and move the trees:
	void inOrderTraversal(FruitNode* current);
	void preOrderTraversal(FruitNode* current);
	void postOrderTraversal(FruitNode* current);

	// ADD the show function prototype:
	void show(FruitNode* p);
};

