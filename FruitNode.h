#pragma once
#include <iostream>
#include <string>

using namespace std;

class FruitNode
{
public:
	int fruitPrice;
	string name;

	FruitNode* leftChild;
	FruitNode* rightChild;

	FruitNode(int fruitPrice, string name);
};

