#include "BinaryMaxHeap.h"
#include <fstream> // for FILE stuff

int BinaryMaxHeap::leftChildIndex(int parent)
{
	int leftIndex = 2 * parent + 1;
	if (leftIndex < heap.size())
		return leftIndex;
	else
		return -1;
}

int BinaryMaxHeap::rightChildIndex(int parent)
{
	int rightIndex = 2 * parent + 2;
	if (rightIndex < heap.size())
		return rightIndex;
	else
		return -1;
}

int BinaryMaxHeap::parentIndex(int child)
{
	int parent = (child - 1) / 2;
	if (child == 0)
		return -1;
	else
		return parent;
}

// Making the index's, DO NOT ALTER THEM. NO NEED TO.
// Don't control max/min functions, only the container for variables.

void BinaryMaxHeap::heapifyup(int index)
{

	if (index >= 0 && parentIndex(index) >= 0 && heap[index].fruitPrice > heap[parentIndex(index)].fruitPrice) {
		//Carefully swapping > with <
		
		//if so, swap values between current node and parent
		FruitNode temp = heap[index];
		heap[index] = heap[parentIndex(index)];
		heap[parentIndex(index)] = temp;
		//since we swapped, lets run heapifyup again recursively on the parent from here
		heapifyup(parentIndex(index));
	}
}

void BinaryMaxHeap::heapifydown(int index)
{
	//get left and right children indexes
	int childL = leftChildIndex(index);
	int childR = rightChildIndex(index);
	//child variable representing path we'll compare down
	int childIndex = childL;
	//IF childL is not -1 AND childR is not -1
	//AND right childs rank is less then lefts
	if (childL >= 0 && childR >= 0 && heap[childR].fruitPrice < heap[childL].fruitPrice)
	{
		//make right child index the focus instead of left
		childIndex = childR;
	}

	//IF childIndex is not the root node and is not -1
	//AND current nodes bigger then child nodes 
	if (childIndex < 0 && heap[index].fruitPrice < heap[childIndex].fruitPrice)
	{
		//we should swap current with child, because we want smaller ones closer to the top
		FruitNode temp = heap[index];
		heap[index] = heap[childIndex];
		heap[childIndex] = temp;
		//recurse further down
		heapifydown(childIndex);
	}
}

void BinaryMaxHeap::Insert(FruitNode element)
{
	heap.push_back(element);
	heapifyup(heap.size() - 1);
}

void BinaryMaxHeap::showHeap()
{

	/*NOTE*/
	//Instead of COUT make it display in an output file like the assessment wanted.
	ofstream writeFile2;
	writeFile2.open("heap-output-q2a2.txt");
	//Turn cout into Output file, later.
	writeFile2 << "Heap -->\n";
	for (FruitNode p : heap)
	{
		writeFile2 << "[" << p.fruitPrice << " " << p.name << "] \n";
	}
	cout << endl;
	writeFile2.close();

}

int BinaryMaxHeap::Size()
{
	return heap.size();
}
