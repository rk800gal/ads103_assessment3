#include "AVL.h"
#include <fstream>

//using recursion, we keep exploring down, and pass final height values up
int AVL::height(FruitNode* node)
{
    int h = 0;
    //helps break recursion cycle when we get to nulls at the bottom of branches
    if (node != NULL)
    {
        int leftH = height(node->leftChild);
        int rightH = height(node->rightChild);

        //max gets biggest of the 2 and discards the smaller
        int maxH = max(leftH, rightH);
        h = maxH + 1;
    }
    return h;
}

int AVL::difference(FruitNode* node)
{
    //if empty treee, well its balanced, its 0
    if (node == NULL)
        return 0;

    int leftH = height(node->leftChild);
    int rightH = height(node->rightChild);
    int balanceFactor = leftH - rightH;

    return balanceFactor;
}

FruitNode* AVL::RRrotation(FruitNode* parent)
{
    FruitNode* temp = parent->rightChild;
    parent->rightChild = temp->leftChild;
    temp->leftChild = parent;
    if (displayRotations)
        cout << "RR rotation on " << parent->name << endl;

    return temp;
}

FruitNode* AVL::LLrotation(FruitNode* parent)
{
    FruitNode* temp = parent->leftChild;
    parent->leftChild = temp->rightChild;
    temp->rightChild = parent;
    if (displayRotations)
        cout << "LL rotation on " << parent->name << endl;

    return temp;
}
//needs 2 rotations,
//first rotation rotates bottom 2 nodes which turns the whole structure into a RR rotation
//second rotation uses RRrotation
FruitNode* AVL::LRrotation(FruitNode* parent)
{
    FruitNode* temp = parent->leftChild;
    parent->leftChild = RRrotation(temp);
    if (displayRotations)
        cout << "LR rotation on " << parent->name << endl;
    return LLrotation(parent);
}

FruitNode* AVL::RLrotation(FruitNode* parent)
{
    FruitNode* temp = parent->rightChild;
    parent->rightChild = LLrotation(temp);
    if (displayRotations)
        cout << "RL rotation on " << parent->name << endl;
    return RRrotation(parent);
}

FruitNode* AVL::balance(FruitNode* parent)
{
    //get balance factor
    int balanceFactor = difference(parent);

    //IF balanceFactor not within -1,0,1, then lets work out what rotations to do
    if (balanceFactor > 1)
    {
        //left branch is heavy, now work out is left or right child heavy
        if (difference(parent->leftChild) > 0)
        {
            //left child unbalanced
            parent = LLrotation(parent);
        }
        else
        {
            //right child unbalanced
            parent = LRrotation(parent);
        }
    }
    else if (balanceFactor < -1)
    {
        //right branch is heavy, but which child
        if (difference(parent->rightChild) > 0)
        {
            //left child heavy
            parent = RLrotation(parent);
        }
        else
        {
            //right child heavy
            parent = RRrotation(parent);
        }
    }


    return parent;
}

FruitNode* AVL::insertAVL(FruitNode* parent, FruitNode* newFruit)
{

    //FIND A WAY TO INSERT STUFF FROM INPUT FILE.

    //if sub tree empty, this becomes the parent
    if (parent == NULL)
    {
        parent = newFruit;
        return parent;
    }

    //parent not null, so we haven't found an empty space to stick new student yet
    //so we need to go down either left or right path
    if (newFruit->fruitPrice < parent->fruitPrice)
    {
        parent->leftChild = insertAVL(parent->leftChild, newFruit);
        parent = balance(parent);
    }
    else //assume id >= parent's id
    {
        parent->rightChild = insertAVL(parent->rightChild, newFruit);
        parent = balance(parent);
    }
}

void AVL::insert(FruitNode* newFruit)
{
    cout << "Inserting " << newFruit->name << " " << newFruit->fruitPrice << endl;
    root = insertAVL(root, newFruit);
    cout << endl;
}

void AVL::showAVL(FruitNode* p)
{

    /*NOTE*/
   //Instead of COUT make it display in an output file like the assessment wanted.
    ofstream writeFile;
    writeFile.open("output-q1-a2.txt");

    // Root is empty, base case.
    if (root == NULL) return;

    // Creates an empty queue for the level order traversal.
    queue<FruitLevelNode> q;

    // Enqueue (add) the root into the queue and initialise the height with the constructor you just built.
    q.push(FruitLevelNode(root, 0));

    int previousOutputLevel = -1;

    while (q.empty() == false)
    {
        //Print the FRONT of the queue and then dequeue (remove) it from the queue.
        FruitLevelNode node = q.front();
        if (node.level != previousOutputLevel)
        {
            writeFile << endl;
            writeFile << node.level << "- ";
            previousOutputLevel = node.level;
        }
        writeFile << node.fruit->fruitPrice << ":" << node.fruit->name << " ";
        q.pop();


        /* For: Enqueing the RIGHT child */
        if (node.fruit->rightChild != NULL)
            q.push(FruitLevelNode(node.fruit->rightChild, node.level + 1));

        /* For: Enqueing the LEFT child */
        //Same thing, replace with the word left.
        if (node.fruit->leftChild != NULL)
            q.push(FruitLevelNode(node.fruit->leftChild, node.level + 1));
    }
    writeFile.close();

}
