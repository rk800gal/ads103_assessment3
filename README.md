$  **Documentation**

My thought process on how everything works, because last time I did not document
anything at all and the project was a mess.

----------------------------------------------------------------------

$ Part 1, Question 1:

What's left to do:
- INPUT FILE.

=======================================================================

First I started working on the Binary Search Tree in order to make
the AVL tree. I'm just writing this to keep progress of what I could be doing.
(Since I have no money/idk how to use Trello without taking hours to find out.)

NOTES:
FruitID is Student ID which I replaced with Fruit PRICE. (fruitPrice)

References used for the BST tree:
(Actually class references, but these are the same anyway.

https://www.geeksforgeeks.org/level-order-tree-traversal/
- The Level Order Binary Tree Traversal from Geeks for Geeks.

- Week 9: Matt's "SHOW TREE LEVELS" guide on Google Drive.

- Week 8: ADS: AVL Theory Google Drive guide.

=======================================================================

_What's inside each .CPP or.h_
_(the important ones that I've modified at least):_

- In the .BST's (BinarySearchTree.h and BinarySearchTree.cpp I've modified some lines for the output.txt file)
- Instead of StudentNode ALL files have been updated from StudentNode -> to FruitNode just FYI.

-- `NOTE` --
In Source.cpp I forgot to uncomment this line of code:
avl1.showAVL(avl1.root);

We need this to output the contents of the algorithm, and I forgot to uncomment it, just an FYI.


-----------------------------------------------------------------------

$  **Documentation**

$ Part 2, Question 2:

What's left to do:
- Make the entire thing
- Commit it here
- FIX INPUT FILE.

=======================================================================

