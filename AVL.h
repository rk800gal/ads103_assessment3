#pragma once
#include "BinarySearchTree.h"

//AVL is self balancing binary search tree
//AVL - Adelson, Velski & Landis
class AVL : public BinarySearchTree
{
public:
	bool displayRotations = true;

	//works out height of sub tree
	int height(FruitNode* node);

	//difference between left and right sub trees
	int difference(FruitNode* node);

	//ROTATIONS
	//return: new parent of subtree
	//parameter: current parent of sub tree
	//right branch, right child
	FruitNode* RRrotation(FruitNode* parent);
	//left branch, left child
	FruitNode* LLrotation(FruitNode* parent);
	//left branch, right child
	FruitNode* LRrotation(FruitNode* parent);
	//right branch, left child
	FruitNode* RLrotation(FruitNode* parent);

	//BALANCE
	//balances a tree structure where parent is the middle top node
	//returns new parent after balancing(rotations)
	FruitNode* balance(FruitNode* parent);

	//INSERT
	//recursive insert that considers parent a sub tree
	//this insert also balances itself
	//returns the new root node of the tree
	FruitNode* insertAVL(FruitNode* parent, FruitNode* newFruit);

	//overriding insert from parent
	void insert(FruitNode* newFruit);

	// ADD the show function prototype
	/* Did this for BST, will do it for AVL.*/
	void showAVL(FruitNode* p);
};

