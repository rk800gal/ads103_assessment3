#pragma once
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
#include "FruitNode.h"

using namespace std;
class BinaryMaxHeap
{

public:
	vector<FruitNode> heap;
	int leftChildIndex(int parent);
	int rightChildIndex(int parent);
	int parentIndex(int child);

	void heapifyup(int index);
	void heapifydown(int index);

	void Insert(FruitNode element);
	/*void DeleteMin(); // We don't really need this for the 
	FruitNode* ExtractMin();*/ // Assessment, and it takes time to build.
	void showHeap();
	int Size();

	// Alt entah. :}
};

